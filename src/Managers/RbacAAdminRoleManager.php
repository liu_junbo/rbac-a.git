<?php


namespace App\Managers\RbacA;

use App\Components\Common\Log\GLogger;
use App\Components\Common\UtilsFunction;
use App\Components\Common\UtilsConst;
use App\Components\SDK\CacheTool\CacheManager;
use App\Managers\Common\StoreManager;
use App\Models\RbacA\RbacAAdminRole;
use App\Components\Project\ProjectTable;
use Illuminate\Support\Arr;
/**
 * Class RbacAAdminRoleManager
 * @package App\Managers\RbacA
 *
 * User: ldawn
 * Date: 2021-9-27
 *
 * @method \App\Managers\Common\StoreManager getById($id)
 * @method \App\Managers\Common\StoreManager getByIdWithTrashed($id)
 * @method \App\Managers\Common\StoreManager save($info)
 * @method \App\Managers\Common\StoreManager batchDelete($ids_arr)
 * @method \App\Managers\Common\StoreManager batchInsert($data_arr)
 * @method \App\Managers\Common\StoreManager batchUpdate($ids_arr, $update_data_arr)
 * @method \App\Managers\Common\StoreManager setNum($info, $item, $num)
 * @method \App\Managers\Common\StoreManager getLatest()
 *
 */
class RbacAAdminRoleManager
{
    const  MODULAR = 'rbac_a_admin_role';
   /*
    * getInfoByLevel
    *
    * By ldawn
    *
    * 2021/8/6
    *
    */
    public static function getInfoByLevel($info, $level)
    {
        //如果$info是null的话，那么需要返回null
        if ($info == null) {
            return null;
        }

        $level_arr = explode(',', $level);

        $info->status_str = Arr::get(UtilsConst::COMMON_STATUS_ENUM,$info->status);
        $info->img_arr=[];
        if ($info->img) {
            $info->img_arr = explode(",", $info->img);
        }


        //if (in_array('user', $level_arr)) {

        //}




        //T:        结束时间字段预设
        if (strpos($level, 'T') !== false) {
        }

        //X:        脱敏
        if (strpos($level, 'X') !== false) {

        }
        //Y:        压缩，去掉content_html等大报文信息
        if (strpos($level, 'Y') !== false) {
            unset($info->content_html);
            unset($info->updated_at);
            unset($info->deleted_at);

        }
        //Z:        预留
        if (strpos($level, 'Z') !== false) {

        }


        return $info;
    }

   /*
    * getListByCon
    *
    * By ldawn
    *
    * 2021/8/6
    *
    * @param        con_arr：条件数组
    * @param        is_painate：true/false，代表是否分页
    * @param        is_stmt：true/false，代表是否统计，所谓统计为->count();->sum('item')，is_stmt=true可以提升统计效率，一般用于统计服务
    * @param        trashed_scope：软删除范围 null：不搜索软删除；withTrashed为带被软删除的信息；onlyTrashed为只返回被软删除的信息
    *
    * return $infos
    */
    public static function getListByCon($con_arr, $is_paginate = true,$is_stmt = false,$trashed_scope = null)
    {
        $infos = new RbacAAdminRole();
        if (array_key_exists('with', $con_arr) && !UtilsFunction::isObjNull($con_arr['with'])) {
            foreach ($con_arr['with'] as $with) {
                $infos = $infos->with($with);
            }
        };
        switch ($trashed_scope) {
            case "withTrashed":
                $infos = $infos->withTrashed();
                break;
            case "onlyTrashed":
                $infos = $infos->onlyTrashed();
                break;
            default:
                break;
        }

        if (array_key_exists('search_word', $con_arr) && !UtilsFunction::isObjNull($con_arr['search_word'])) {
            $keyword = $con_arr['search_word'];
            $infos = $infos->where(function ($query) use ($keyword) {
                $query->where('name', 'like', "%{$keyword}%")
                    ->orwhere('id', 'like', "%{$keyword}%");
            });
        }

        //2020年3月4日，此处优化empty
        if (array_key_exists('ids_arr', $con_arr) && !UtilsFunction::isObjNull($con_arr['ids_arr'])) {
            $infos = $infos->wherein('id', $con_arr['ids_arr']);
        }

        if (array_key_exists('not_id', $con_arr) && !UtilsFunction::isObjNull($con_arr['not_id'])) {
            $infos = $infos->where('id','<>', $con_arr['not_id']);
        }

    
        if (array_key_exists('id', $con_arr) && !UtilsFunction::isObjNull($con_arr['id'])) {
            $infos = $infos->where('id', '=', $con_arr['id']);
        }
    
        if (array_key_exists('admin_id', $con_arr) && !UtilsFunction::isObjNull($con_arr['admin_id'])) {
            $infos = $infos->where('admin_id', '=', $con_arr['admin_id']);
        }
    
        if (array_key_exists('role_id', $con_arr) && !UtilsFunction::isObjNull($con_arr['role_id'])) {
            $infos = $infos->where('role_id', '=', $con_arr['role_id']);
        }
    
        if (array_key_exists('status', $con_arr) && !UtilsFunction::isObjNull($con_arr['status'])) {
            $infos = $infos->where('status', '=', $con_arr['status']);
        }
    
        if (array_key_exists('seq', $con_arr) && !UtilsFunction::isObjNull($con_arr['seq'])) {
            $infos = $infos->where('seq', '=', $con_arr['seq']);
        }
    
        if (array_key_exists('created_at', $con_arr) && !UtilsFunction::isObjNull($con_arr['created_at'])) {
            $infos = $infos->where('created_at', '=', $con_arr['created_at']);
        }
    
        if (array_key_exists('updated_at', $con_arr) && !UtilsFunction::isObjNull($con_arr['updated_at'])) {
            $infos = $infos->where('updated_at', '=', $con_arr['updated_at']);
        }
    
        if (array_key_exists('deleted_at', $con_arr) && !UtilsFunction::isObjNull($con_arr['deleted_at'])) {
            $infos = $infos->where('deleted_at', '=', $con_arr['deleted_at']);
        }
    
        //2020年3月3日进行优化，如果是is_stmt==true，则直接返回$infos，并且可以直接追加sum('total_fee')等字段
        if($is_stmt == true){
            return $infos;
        }

        //排序设定
        if (array_key_exists('orderby', $con_arr) && is_array($con_arr['orderby'])) {
            $orderby_arr = $con_arr['orderby'];
            //例子，传入数据样式为'status'=>'desc'
            if (array_key_exists('status', $orderby_arr) && !UtilsFunction::isObjNull($orderby_arr['status'])) {
                $infos = $infos->orderby('status', $orderby_arr['status']);
            }
            //如果传入random，代表要随机获取
            if (array_key_exists('random', $orderby_arr) && !UtilsFunction::isObjNull($orderby_arr['random'])) {
                $infos = $infos->inRandomOrder();
            }
        }
        $infos = $infos->orderby('seq', 'desc')->orderby('id', 'desc');

        //分页设定
        if ($is_paginate) {
            $page_size = UtilsConst::PAGE_SIZE;
            //如果con_arr中有page_size信息
            if (array_key_exists('page_size', $con_arr) && !UtilsFunction::isObjNull($con_arr['page_size'])) {
                $page_size = $con_arr['page_size'];
            }
            $infos = $infos->paginate($page_size);
        }
        else {
            //如果con_arr中有page_size信息 2019-10-08优化，可以不分页也获取多条数据
            if (array_key_exists('page_size', $con_arr) && !UtilsFunction::isObjNull($con_arr['page_size'])) {
                $page_size = $con_arr['page_size'];
                $infos = $infos->take($page_size);
            }
            $infos = $infos->get();
        }
        return $infos;
    }

   /*
    * setInfo
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public static function setInfo($info, $data)
    {

                if (array_key_exists('id', $data) && !UtilsFunction::isObjNull($data['id'])) {
                $info->id = $data['id'];
            }
                if (array_key_exists('admin_id', $data) && !UtilsFunction::isObjNull($data['admin_id'])) {
                $info->admin_id = $data['admin_id'];
            }
                if (array_key_exists('role_id', $data) && !UtilsFunction::isObjNull($data['role_id'])) {
                $info->role_id = $data['role_id'];
            }
                if (array_key_exists('status', $data) && !UtilsFunction::isObjNull($data['status'])) {
                $info->status = $data['status'];
            }
                if (array_key_exists('seq', $data) && !UtilsFunction::isObjNull($data['seq'])) {
                $info->seq = $data['seq'];
            }
                if (array_key_exists('created_at', $data) && !UtilsFunction::isObjNull($data['created_at'])) {
                $info->created_at = $data['created_at'];
            }
                if (array_key_exists('updated_at', $data) && !UtilsFunction::isObjNull($data['updated_at'])) {
                $info->updated_at = $data['updated_at'];
            }
                if (array_key_exists('deleted_at', $data) && !UtilsFunction::isObjNull($data['deleted_at'])) {
                $info->deleted_at = $data['deleted_at'];
            }
        
        return $info;
    }


    public static function __callStatic($method, $parameters)
    {
        return (new StoreManager(self::MODULAR))->{$method}(...$parameters);
    }
}
