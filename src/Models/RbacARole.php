<?php

/**
* Created by PhpStorm.
* User: ldawn
* Date: 2021/8/6
*/
namespace App\Models\RbacA;

use App\Models\Common\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class RbacARole extends BaseModel
{
    use SoftDeletes;
    protected $table ='rbac_a_role';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    /*
    * 进行类型转换
    *
    * @var  array
    *
    */
    protected $casts = [
            ];

}

