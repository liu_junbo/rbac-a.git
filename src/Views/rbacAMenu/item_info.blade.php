<style>

</style>
<div class="layui-card-body" pad15>
    <div class="layui-form" lay-filter="">

        @include('component.form.input_hidden',['c_name'=>'id','c_value'=>$data->id])

        @include('component.form.input',['c_title'=>'名称','c_name'=>'name','c_value'=>$data->name,'c_desc'=>'不可为空，请输入名称'])


        @include('component.form.input_select',['c_title'=>'父级菜单','c_name'=>'father','c_id_value'=>$data->father_id,
      'c_name_value'=>isset($data->menu->name)?$data->menu->name:"",'c_url'=>URL::asset('/admin/rbacAMenu/select?select_type=menu'),'c_verify'=>''])

        @include('component.form.input_select',['c_title'=>'绑定权限','c_name'=>'power','c_id_value'=>$data->power_id,
      'c_name_value'=>isset($data->power->name)?$data->power->name:"",'c_url'=>URL::asset('/admin/rbacAPower/select?select_type=power'),'c_verify'=>''])


        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="LAY-form-edit">保存信息</button>
                <button type="reset" class="layui-btn layui-btn-primary" onclick="reloadPage();">
                    重新填写
                </button>
                <button class="layui-btn layui-btn-primary" onclick="back();">
                    返回
                </button>
            </div>
        </div>
    </div>
</div>
@include('component.form.init',['form_url'=>URL::asset('/admin/'.$modular.'/editInfoPost')])

<script type="text/javascript">

</script>
