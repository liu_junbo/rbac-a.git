@foreach($menu_list as $key=>$one_menu)
    <li data-name="component" class="layui-nav-item">
        <a href={{!empty($one_menu['url'])?$one_menu['url']:"javascript:;"}} lay-tips="{{$one_menu['name']}}">
            <i class="layui-icon layui-icon-user"></i>
            <cite>{{$one_menu['name']}}</cite>
        </a>
        @foreach($one_menu['son_menu'] as $two_menu)
            <dl class="layui-nav-child">
                <dd @if(!$two_menu['url']) data-name="button" @endif>
                    @if($two_menu['url'])
                        <a lay-href="{{ asset($two_menu['url'])}}">{{$two_menu['name']}}</a>
                    @else
                        <a href="javascript:;">{{$two_menu['name']}}</a>
                    @endif
                    @foreach($two_menu['son_menu'] as $three_menu)
                        <dl class="layui-nav-child">
                            <dd data-name="button">
                                <a lay-href="{{ asset($three_menu['url'])}}">{{$three_menu['name']}}</a>
                            </dd>
                        </dl>
                    @endforeach
                </dd>
            </dl>
        @endforeach
    </li>
@endforeach

