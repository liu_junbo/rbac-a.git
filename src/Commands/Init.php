<?php

namespace Ldawn\RbacA\Commands;

use App\Components\Project\ProjectTable;
use App\Managers\Common\AdminManager;
use App\Managers\Common\ColumnsManager;
use App\Managers\RbacA\RbacAMenuManager;
use App\Managers\RbacA\RbacAPowerManager;
use App\Managers\RbacA\RbacARoleManager;
use App\Managers\RbacA\RbacARolePowerManager;
use App\Models\RbacA\RbacAPower;
use App\Models\RbacA\RbacAMenu;
use App\Models\RbacA\RbacARole;
use App\Models\RbacA\RbacARolePower;
use Illuminate\Console\Command;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Router;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Managers\RbacA\RbacAAdminRoleManager;
use App\Models\RbacA\RbacAAdminRole;
use Ldawn\Base\Store\BaseStore;

class Init extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rbac-a:init
     {--p=admin : 平台}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    const RBACA_SRC_PATH='vendor\ldawn\rbac-a/src';

    const INIT_MENU = [
        ['name' => '用户管理',
            'son_menu' => [
                ['name' => '管理员管理', 'url' => 'admin/admin/index']
            ]],
        ['name' => 'rbac管理',
            'son_menu' => [
                ['name' => '权限管理', 'url' => 'admin/rbacAPower/index'],
                ['name' => '角色管理', 'url' => 'admin/rbacARole/index'],
                ['name' => '菜单管理', 'url' => 'admin/rbacAMenu/index'],
            ]],
    ];

    const PUBLIC_VIEW_FILE_ARR=[
        'rbacAAdminPower\add_power.blade.php',
        'rbacAAdminPower\cut_power.blade.php',
        'rbacAAdminPower\total_power.blade.php',
        'rbacAMenu\edit.blade.php',
        'rbacAMenu\index.blade.php',
        'rbacAMenu\item_info.blade.php',
        'rbacAMenu\menu.blade.php',
        'rbacAMenu\select.blade.php',
        'rbacAPower\edit.blade.php',
        'rbacAPower\index.blade.php',
        'rbacAPower\item_info.blade.php',
        'rbacAPower\select.blade.php',
        'rbacARole\edit.blade.php',
        'rbacARole\index.blade.php',
        'rbacARole\item_info.blade.php',
        'rbacARole\select.blade.php',
        'component\checkbox_power.blade.php',
    ];

    const PUBLIC_CONTROLLER_FILE_ARR=[
        'RbacAAdminPowerController.php',
        'RbacAAdminRoleController.php',
        'RbacAMenuController.php',
        'RbacAPowerController.php',
        'RbacARoleController.php',
        'RbacARolePowerController.php',
    ];

    const PUBLIC_MODELS_FILE_ARR=[
        'RbacAAdminPower.php',
        'RbacAAdminRole.php',
        'RbacAMenu.php',
        'RbacAPower.php',
        'RbacARole.php',
        'RbacARolePower.php',
    ];

    const PUBLIC_MANAGERS_FILE_ARR=[
        'RbacAAdminPowerManager.php',
        'RbacAAdminRoleManager.php',
        'RbacAMenuManager.php',
        'RbacAPowerManager.php',
        'RbacARoleManager.php',
        'RbacARolePowerManager.php',
    ];

    public $no_power_modular;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Router $routes)
    {
        if(config('app.env')=='local') {
            BaseStore::resetConfig('rbac-a');

            BaseStore::createProjectTable();

            self::publicController();

            self::publicView();

            self::publicComposer();

            self::publicModel();

            self::publicManager();
        }

        self::createPower($routes);

        self::initRoleAndMenu();
    }


    private function createPower($routes)
    {
        $power_platform = RbacAPowerManager::getListByCon(['platform' => $this->option('p')], false, true)->first();
        if (!$power_platform) {
            $power_platform = new RbacAPower();
            $power_platform->name = '最高权限';
            $power_platform->type = 1;
            $power_platform->platform = $this->option('p');
            $power_platform->save();
        }

        $power_urls=RbacAPowerManager::getListByCon(['have_url'=>1],false)->pluck('url')->toArray();
        $power_urls=array_flip($power_urls);

        $power_modulars=RbacAPowerManager::getListByCon(['type=>2'],false)->toArray();

        $power_modular_arr=[];
        foreach ($power_modulars as $power_modular){
            $power_modular_arr[$power_modular['modular']]=$power_modular['id'];
        };
        $routes = $routes->getRoutes();
        foreach ($routes as $k=>$route){
            if (Str::is( $this->option('p').'/*', $route->uri)) {
                $url_arr = explode('/', $route->uri);
                if (in_array($url_arr[1],config('ldawn.rbac-a.no_power_modular'))) {
                    continue;
                }
                if(!Arr::get($power_modular_arr, $url_arr[1])){
                    $power = new RbacAPower();
                    $power->name = Arr::get(config('ldawn.modular_list'),$url_arr[1]);
                    $power->type = 2;
                    $power->modular = $url_arr[1];
                    $power->father_id =$power_platform->id;
                    $power->save();
                    $power_modular_arr[$power->modular]=$power->id;
                }

                if (Arr::get($power_urls, $route->uri)===null) {
                    $power = new RbacAPower();
                    if(!Arr::get($url_arr,2)){
                        continue;
                    }
                    $power->name =Arr::get(config('ldawn.opt_list'),$url_arr[2]);
                    $power->type = 3;
                    $power->modular = $url_arr[1];
                    $power->father_id = Arr::get($power_modular_arr,$url_arr[1]);
                    $power->url = $route->uri;
                    $power->save();
                }
            }
        }
    }

    private function publicController()
    {
        foreach (self::PUBLIC_CONTROLLER_FILE_ARR as $public_controller_file) {
            if(!Storage::disk('base')->exists("app/Http/Controllers/Web/Admin/RbacA/".$public_controller_file)){
                Storage::disk('base')->copy(self::RBACA_SRC_PATH . '/Controllers/'.$public_controller_file, "app/Http/Controllers/Web/Admin/RbacA/".$public_controller_file);
            }
        }
    }

    private function publicView()
    {
        foreach (self::PUBLIC_VIEW_FILE_ARR as $public_view_file) {
            if(!Storage::disk('base')->exists("resources/views/vendor/rbacA/".$public_view_file)){
                Storage::disk('base')->copy(self::RBACA_SRC_PATH . '/Views/'.$public_view_file, "resources/views/vendor/rbacA/".$public_view_file);
            }
        }
    }

    private function publicComposer()
    {
        if (!Storage::disk('base')->exists("app/View/Composer/AdminLeftMenu.php")) {
            Storage::disk('base')->copy(self::RBACA_SRC_PATH . '/Composer/AdminLeftMenu.php', "app/View/Composer/AdminLeftMenu.php");
        }
    }

    private function publicModel()
    {
        foreach (self::PUBLIC_MODELS_FILE_ARR as $public_model_file) {
            if(!Storage::disk('base')->exists("app/Models/RbacA/".$public_model_file)){
                Storage::disk('base')->copy(self::RBACA_SRC_PATH . '/Models/'.$public_model_file, "app/Models/RbacA/".$public_model_file);
            }
        }
    }

    private function publicManager()
    {
        foreach (self::PUBLIC_MANAGERS_FILE_ARR as $public_manager_file) {
            if(!Storage::disk('base')->exists("app/Managers/RbacA/".$public_manager_file)){
                Storage::disk('base')->copy(self::RBACA_SRC_PATH . '/Managers/'.$public_manager_file, "app/Managers/RbacA/".$public_manager_file);
            }
        }
    }

    private function initRoleAndMenu()
    {
        $role=RbacARoleManager::getListByCon([],false,true)->first();
        if(!$role) {
            $role = new RbacARole();
            $role = RbacARoleManager::setInfo($role, ['name' => '最高管理员']);
            RbacARoleManager::save($role);

            $admin = AdminManager::getListByCon(['orderby' => ['id' => 'asc']], false)->first();
            $admin_role = new RbacAAdminRole();
            $admin_role = RbacAAdminRoleManager::setInfo($admin_role, ['admin_id' => $admin->id, 'role_id' => $role->id]);
            RbacAAdminRoleManager::save($admin_role);

            $power = RbacAPowerManager::getListByCon(['orderby' => ['id' => 'asc']], false)->first();
            $role_power = new RbacARolePower();
            $role_power = RbacARolePowerManager::setInfo($role_power, ['power_id' => $power->id, 'role_id' => $role->id]);
            RbacARolePowerManager::save($role_power);

            $power_urls=RbacAPowerManager::getListByCon(['have_url'=>1],false)->toArray();
            $power_url_id_arr=[];
            foreach ($power_urls as $power_url){
                $power_url_id_arr[$power_url['url']]=$power_url['id'];
            }

            foreach (self::INIT_MENU as $menu_list){
                $menu=RbacAMenuManager::getListByCon(['name'=>$menu_list['name'],'father_id'=>0],false,true)->first();
                if(!$menu){
                    $menu=new RbacAMenu();
                    $menu=RbacAMenuManager::setInfo($menu,['father_id'=>0,'name'=>$menu_list['name']]);
                    RbacAMenuManager::save($menu);
                }
                foreach ($menu_list['son_menu'] as $son_menu_item){
                    $son_menu=RbacAMenuManager::getListByCon(['power_id'=>$power_url_id_arr[$son_menu_item['url']],'father_id'=>$menu->id],false,true)->first();
                    if(!$son_menu){
                        $son_menu=new RbacAMenu();
                        $son_menu=RbacAMenuManager::setInfo($son_menu,['power_id'=>$power_url_id_arr[$son_menu_item['url']],'father_id'=>$menu->id,'name'=>$son_menu_item['name']]);
                        RbacAMenuManager::save($son_menu);
                    }
                }
            }
        }
    }
}
