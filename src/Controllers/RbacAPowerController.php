<?php

namespace  App\Http\Controllers\Web\Admin\RbacA;

use App\Components\Common\RequestValidator;
use App\Managers\RbacA\RbacAPowerManager;
use App\Components\Common\QNManager;
use App\Components\Common\UtilsFunction;
use App\Components\Common\ApiResponse;
use App\Models\RbacA\RbacAPower;
use Illuminate\Http\Request;

class RbacAPowerController
{
    const  MODULAR='rbacAPower';

    /*
    * 首页
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function index(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //相关搜素条件
        $status = null;
        $search_word = null;
        $modular = null;
        if (array_key_exists('status', $data) && !UtilsFunction::isObjNull($data['status'])) {
            $status = $data['status'];
        }
        if (array_key_exists('search_word', $data) && !UtilsFunction::isObjNull($data['search_word'])) {
            $search_word = $data['search_word'];
        }
        if (array_key_exists('modular', $data) && !UtilsFunction::isObjNull($data['modular'])) {
            $modular = $data['modular'];
        }
        $con_arr = array(
            'status' => $status,
            'search_word' => $search_word,
            'modular' => $modular,
        );
        $rbac_a_powers =RbacAPowerManager::getListByCon($con_arr, true);
        foreach ($rbac_a_powers as $rbac_a_power) {
            $rbac_a_power = RbacAPowerManager::getInfoByLevel($rbac_a_power, '');
        }

        return view('RbacA::rbacAPower.index', ['self_admin' => $self_admin, 'datas' => $rbac_a_powers, 'con_arr' => $con_arr,'modular'=>self::MODULAR]);
    }

    /*
    * 编辑-get
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function edit(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        $item=0;
        //生成七牛token
        $upload_token = QNManager::uploadToken();
        $rbac_a_power = new RbacAPower();
        if (array_key_exists('id', $data)) {
            $rbac_a_power = RbacAPowerManager::getById($data['id']);
        }
        $rbac_a_power = RbacAPowerManager::getInfoByLevel($rbac_a_power, "father");
        if (array_key_exists('item', $data)) {
            $item=$data['item'];
        }
        return view('RbacA::rbacAPower.edit', ['self_admin' => $self_admin, 'data' => $rbac_a_power, 'upload_token' => $upload_token, 'item' => $item,'modular'=>self::MODULAR]);
    }


    /*
    * 添加或编辑-post
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function editInfoPost(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //默认赋值
        $data['admin_id']=$self_admin->id;
        $rbac_a_power = new RbacAPower();
        //存在id是保存
        if (array_key_exists('id', $data) && !UtilsFunction::isObjNull($data['id'])) {
            $rbac_a_power = RbacAPowerManager::getById($data['id']);
        }
        $data['admin_id'] = $self_admin['id'];
        $rbac_a_power = RbacAPowerManager::setInfo($rbac_a_power, $data);
        RbacAPowerManager::save($rbac_a_power);
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, $rbac_a_power);
    }


    /*
    * 设置状态
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function setStatus(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //合规校验
        $requestValidationResult = RequestValidator::validator($request->all(), [
            'id' => 'required',
            'status'=>'required'
        ]);
        if ($requestValidationResult !== true) {
            return ApiResponse::makeResponse(ApiResponse::MISSING_PARAM, $requestValidationResult);
        }
        $rbac_a_power = RbacAPowerManager::getById($data['id']);
        if (!$rbac_a_power) {
            return ApiResponse::makeResponse(ApiResponse::INNER_ERROR, null, "未找到信息");
        }
        $rbac_a_power = RbacAPowerManager::setInfo($rbac_a_power, $data);
        RbacAPowerManager::save($rbac_a_power);
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, $rbac_a_power, "设置成功");
    }

    /*
    * 设置排序
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function setSeq(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //默认赋值
        $data['admin_id']=$self_admin->id;
        //合规校验
        $requestValidationResult = RequestValidator::validator($request->all(), [
            'id' => 'required'
        ]);
        if ($requestValidationResult !== true) {
            return ApiResponse::makeResponse(ApiResponse::MISSING_PARAM, $requestValidationResult);
        }
        $rbac_a_power = RbacAPowerManager::getById($data['id']);
        if (!$rbac_a_power) {
            return ApiResponse::makeResponse(ApiResponse::INNER_ERROR, null, "未找到信息");
        }
        $rbac_a_power = RbacAPowerManager::setInfo($rbac_a_power, $data);
        RbacAPowerManager::save($rbac_a_power);
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, $rbac_a_power, "设置成功");
    }



    /*
    * 删除
    *
    * By junbo
    *
    * 2019-05-18 17:14:16
    */
    public function del(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //合规校验
        $requestValidationResult = RequestValidator::validator($request->all(), [
            'id' => 'required',
        ]);
        if ($requestValidationResult !== true) {
            return ApiResponse::makeResponse(ApiResponse::MISSING_PARAM, $requestValidationResult);
        }
        $rbac_a_power = RbacAPowerManager::getById($data['id']);
        if (!$rbac_a_power) {
            return ApiResponse::makeResponse(ApiResponse::INNER_ERROR, null, "未找到信息");
        }
        RbacAPowerManager::batchDelete([$data['id']]);
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, null, "删除成功");
    }

    /*
    * 批量删除
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function batchDel(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //合规校验
        $requestValidationResult = RequestValidator::validator($request->all(), [
            'ids' => 'required',
        ]);
        if ($requestValidationResult !== true) {
            return ApiResponse::makeResponse(ApiResponse::MISSING_PARAM, $requestValidationResult);
        }
        $ids_arr = explode(',',rtrim($data['ids'],','));
        RbacAPowerManager::batchDelete($ids_arr);
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, null, "删除成功");
    }



    /*
    * 选择页
    *
    * By ldawn
    *
    * 2021/8/6
    *
    */
    public function select(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //相关搜素条件
        $status = null;
        $search_word=null;
        $other_id=null;
        $select_type=null;
        $modular=null;
        if (array_key_exists('status', $data) ) {
            $status = $data['status'];
        }
        if (array_key_exists('search_word', $data) ) {
            $search_word = $data['search_word'];
        }
        if (array_key_exists('other_id', $data)) {
            $other_id = $data['other_id'];
        }
        if (array_key_exists('select_type', $data)) {
            $select_type = $data['select_type'];
        }
        if (array_key_exists('modular', $data)) {
            $modular = $data['modular'];
        }
        $con_arr = array(
            'status' => $status,
            'search_word' => $search_word,
            'other_id' => $other_id,
            'select_type' => $select_type,
            'modular' => $modular,
        );
        if ($modular == 'menu') {
            $con_arr['type'] = 3;
        }

        $rbac_a_powers =RbacAPowerManager::getListByCon($con_arr, true);
        foreach ($rbac_a_powers as $rbac_a_power) {
            $rbac_a_power = RbacAPowerManager::getInfoByLevel($rbac_a_power, '');
        }

        return view('RbacA::rbacAPower.select', ['self_admin' => $self_admin, 'datas' => $rbac_a_powers, 'con_arr' => $con_arr,'modular'=>self::MODULAR]);
    }

        /*
        * 排序编辑页
        *
        * By ldawn
        *
        * 2021/8/6
        *
        */
        public function seqEdit(Request $request)
        {
            $data = $request->all();
            $self_admin = $request->session()->get('self_admin');
            return view('admin.rbacAPower.seq_edit', ['self_admin' => $self_admin, 'seq_type' => $data['seq_type'],'modular'=>self::MODULAR]);
        }

        /*
        * 排序保存
        *
        * By ldawn
        *
        * 2021/8/6
        *
        */
        public function seqEditPost(Request $request)
        {
            $data = $request->all();
            $self_admin = $request->session()->get('self_admin');
            //默认赋值
            $data['admin_id']=$self_admin->id;
            //合规校验
            $requestValidationResult = RequestValidator::validator($request->all(), [
                'id' => 'required',
                'seq_type'=>'required'
            ]);
            if ($requestValidationResult !== true) {
                return ApiResponse::makeResponse(ApiResponse::MISSING_PARAM, $requestValidationResult);
            }
            $rbac_a_power = RbacAPowerManager::getById($data['$rbac_a_power_id']);
            if (!$rbac_a_power) {
            return ApiResponse::makeResponse(ApiResponse::INNER_ERROR, null, "未找到信息");
            }

            RbacAPowerManager::setInfo($rbac_a_power,$data);
            RbacAPowerManager::save($rbac_a_power);
            return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, $rbac_a_power, "设置成功");
        }
}


